## Version 4 Breaking Changes

- Dropped TYPO3 9, 10 and 11 support

## Version 3 Breaking Changes

- Dropped TYPO3 8 support

<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "sg_contentlink".
 *
 * Auto generated 11-06-2015 13:57
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['sg_contentlink'] = [
	'title' => 'sgalinski Contentlink',
	'description' => 'Possibility to set a link around a whole content element. (Include static template)',
	'category' => 'plugin',
	'version' => '5.0.2',
	'state' => 'stable',
	'author' => 'Fabian Galinski',
	'author_email' => 'fabian@sgalinski.de',
	'author_company' => 'sgalinski Internet Services (https://www.sgalinski.de)',
	'constraints' =>
		[
			'depends' =>
				[
					'typo3' => '12.5.0.22.4.99',
					'php' => '8.1.0-8.3.99',
				],
			'conflicts' =>
				[
				],
			'suggests' =>
				[
				],
		],
];

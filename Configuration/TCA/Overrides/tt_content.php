<?php

use SGalinski\SgContentlink\TCA\TcaProvider;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

ExtensionManagementUtility::addTCAcolumns(
	'tt_content',
	[
		'tx_sgcontentlink_contentlink' => [
			'displayCond' => TcaProvider::getAllowedTypesForTcaDisplayCond(),
			'exclude' => 1,
			'label' => 'LLL:EXT:sg_contentlink/Resources/Private/Language/locallang_db.xlf:tt_content.tx_sgcontentlink_contentlink',
			'config' => [
				'type' => 'input',
				'renderType' => 'inputLink'
			]
		]
	]
);

ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--linebreak--LLL:EXT:sg_contentlink/Resources/Private/Language/locallang_db.xlf:tt_content.tx_sgcontentlink_contentlink,tx_sgcontentlink_contentlink',
	'',
	'after:subheader'
);

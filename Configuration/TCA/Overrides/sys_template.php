<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

ExtensionManagementUtility::addStaticFile(
	'sg_contentlink',
	'Configuration/TypoScript/Frontend',
	'Contentlink'
);
